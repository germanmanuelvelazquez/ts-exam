# Ejercicio: Factorializer
En este ejercicio se deberá desarrollar el método `factorialize` definido en la clase `Factorializer` ("./factorializer.ts") el cual deberá resolver el factorial del número `n` pasado por parámetro, según las siguientes reglas:

El factorial de un entero positivo n, el factorial de n o n factorial se define en principio como el producto de todos los números enteros positivos desde 1 (es decir, los números naturales) hasta n. Por ejemplo:

$$
5! = 5 * 4 * 3 * 2 * 1 = 120
$$

## Condiciones
### Parte 1
1. Realizar lo necesario para que los tests ("./test.ts") se ejecuten al escribir por línea de comando: `ts-node ./test.ts` ([Ver módulo ts-node](https://www.npmjs.com/package/ts-node)).
2. El archivo "./test.ts" no puede ser modificado.
3. La ejecución de los tests debe mostrar "OK!! :)" por consola.

### Parte 2
1. Realizar lo necesario para que los tests ("./test.ts") se ejecuten al escribir por linea de comando `npm run test` y que no sea necesario que `ts-node` esté instalado globalmente.
2. Realizar lo necesario para que el código se transpile al ejecutar `npm run build`.
3. Realizar lo necesario para que el código transpilado quede en una carpeta llamada `dist`.