import { Factorializer } from './factorializer';

const factorializer = new Factorializer();

if(factorializer.factorialize(5) !== 120) {
    throw 'El factorial de 5 debe ser 120';
}

if(factorializer.factorialize(4) !== 24) {
    throw 'El factorial de 4 debe ser 24';
}

if(factorializer.factorialize(3) !== 6) {
    throw 'El factorial de 3 debe ser 6';
}

if(factorializer.factorialize(2) !== 2) {
    throw 'El factorial de 2 debe ser 2';
}

if(factorializer.factorialize(1) !== 1) {
    throw 'El factorial de 1 debe ser 1';
}

if(factorializer.factorialize(0) !== 1) {
    throw 'El factorial de 0 debe ser 1';
}

console.log('OK!! :)');